# **RESTAURANT REST API**

## **Table of content**

* [Requirements](#requirements)
* [How to run](#run)

## **requirements**

* Docker

## **run**

1. Download repo from GitLab:
`https://gitlab.com/KrysPy/restaurantapi.git`

2. Move to the repo and run:
`docker-compose up --build`

You will get swagger docs at: `http://localhost:8080/swagger/index.html`
