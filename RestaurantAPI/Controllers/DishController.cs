﻿using Microsoft.AspNetCore.Mvc;
using RestaurantAPI.Interfaces;
using RestaurantAPI.Models;
using RestaurantAPI.Validators;

namespace RestaurantAPI.Controllers
{
    [ApiController]
    [Route("restaurant/{restaurantId:int}/dish")]
    public class DishController : ControllerBase
    {
        private readonly IDishService _dishService;

        public DishController(IDishService dishService)
        {
            _dishService = dishService;
        }

        [HttpPost]
        public IActionResult CreateDish(int restaurantId, CreateDishDto dto)
        {
            var validator = new CreateDishDtoValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                var errorMsg = validationResult.Errors.Select(x => x.ErrorMessage);
                return BadRequest(new { error =  errorMsg });
            }

            var dish = _dishService.CreateDish(restaurantId, dto);

            if (dish is null)
            {
                return NotFound(new { error = "Restaurant not found" });
            }

            return CreatedAtAction(nameof(GetDishById), new { dishId = dish.Id, restaurantId }, null);
        }

        [HttpGet]
        public ActionResult<IEnumerable<DishDto>> GetAllRestaurantDishes(int restaurantId)
        {
            var dishes = _dishService.GetAllRestaurantDishes(restaurantId);

            if (dishes is null)
            {
                return NotFound(new { error = "Restaurant not found" });
            }
            return Ok(dishes);
        }

        [HttpGet("{dishId:int}")]
        public ActionResult<DishDto> GetDishById(int restaurantId, int dishId) 
        {
            var dish = _dishService.GetDishById(restaurantId ,dishId);

            if (dish is null)
            {
                return NotFound();
            }
            return Ok(dish);
        }

        [HttpPut("{dishId:int}")]
        public IActionResult UpdateDish(int restaurantId, int dishId, UpdateDishDto dto)
        {
            var validator = new UpdateDishDtoValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                var errorMsg = validationResult.Errors.Select(x => x.ErrorMessage);
                return BadRequest(new { error = errorMsg });
            }

            var updateResult = _dishService.UpdateDish(restaurantId, dishId, dto);

            if (!updateResult)
            {
                return NotFound();
            }
            return NoContent();
        }

        [HttpDelete("{dishId:int}")]
        public IActionResult DeleteDish(int restaurantId, int dishId)
        {
            var deleteResult = _dishService.DeleteDish(restaurantId, dishId);

            if (!deleteResult)
            {
                return NotFound();
            }
            return NoContent();
        }

        [HttpDelete]
        public IActionResult DeleteRestaurantDishes(int restaurantId)
        {
            var deleteResult = _dishService.DeleteAllDishes(restaurantId);

            if (!deleteResult)
            {
                return NotFound(new { error = "Restaurant not found" });
            }
            return NoContent();
        }
    }
}
