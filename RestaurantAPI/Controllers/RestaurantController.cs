﻿using Microsoft.AspNetCore.Mvc;
using RestaurantAPI.Interfaces;
using RestaurantAPI.Models;
using RestaurantAPI.Validators;

namespace RestaurantAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RestaurantController : ControllerBase
    {
        private readonly IRestaurantService _restaurantService;

        public RestaurantController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }

        [HttpPost]
        public IActionResult CreateRestaurant(CreateRestaurantDto dto)
        {
            var validator = new CreateRestaurantDtoValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                var errorMsg = validationResult.Errors.Select(x => x.ErrorMessage);
                return BadRequest(new { error = errorMsg });
            }

            var restaurant = _restaurantService.CreateRestaurant(dto);

            return CreatedAtAction(nameof(GetRestaurantById), new { id = restaurant.Id }, null);
        }

        [HttpGet("{id:int}")]
        public ActionResult<RestaurantDto> GetRestaurantById(int id)
        {
            var restaurant = _restaurantService.GetRestaurantById(id);

            if (restaurant is null)
            {
                return NotFound();
            }
            return Ok(restaurant);
        }

        [HttpGet]
        public ActionResult<IEnumerable<RestaurantDto>> GetAllRestaurants()
        {
            var restaurants = _restaurantService.GetAllRestaurants();

            return Ok(restaurants);
        }

        [HttpPut("{id:int}")]
        public IActionResult UpdateRestaurant(int id, UpdateRestaurantDto dto)
        {
            var validator = new UpdateRestaurantDtoValidator();
            var validationResult = validator.Validate(dto);

            if (!validationResult.IsValid)
            {
                var errorMsg = validationResult.Errors.Select(x => x.ErrorMessage);
                return BadRequest(new { error = errorMsg });
            }

            var updateResult = _restaurantService.UpdateRestaurant(id, dto);

            if (!updateResult)
            {
                return NotFound();
            }
            return NoContent();
        }
        
        [HttpDelete("{id:int}")]
        public IActionResult DeleteRestaurant(int id)
        {
            var deleteResult = _restaurantService.DeleteRestaurant(id);

            if (!deleteResult)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
