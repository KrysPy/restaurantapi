﻿using RestaurantAPI.Entities;
using RestaurantAPI.Models;

namespace RestaurantAPI.Interfaces
{
    public interface IRestaurantService
    {
        RestaurantDto CreateRestaurant(CreateRestaurantDto dto);
        RestaurantDto? GetRestaurantById(int id);
        IEnumerable<RestaurantDto> GetAllRestaurants();
        bool UpdateRestaurant(int id, UpdateRestaurantDto dto);
        bool DeleteRestaurant(int id);
    }
}
