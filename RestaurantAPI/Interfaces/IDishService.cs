﻿using RestaurantAPI.Models;

namespace RestaurantAPI.Interfaces
{
    public interface IDishService
    {
        DishDto? CreateDish(int restaurantId, CreateDishDto dto);
        IEnumerable<DishDto>? GetAllRestaurantDishes(int restaurantId);
        DishDto? GetDishById(int restaurantId, int dishId);
        bool UpdateDish(int restaurantId, int dishId, UpdateDishDto dto);
        bool DeleteDish(int restaurantId, int dishId);
        bool DeleteAllDishes(int restaurantId);
    }
}
