﻿using FluentValidation;
using RestaurantAPI.Models;

namespace RestaurantAPI.Validators
{
    public class CreateRestaurantDtoValidator : AbstractValidator<CreateRestaurantDto>
    {
        public CreateRestaurantDtoValidator()
        {
            RuleFor(dto => dto.Name)
                .NotEmpty().WithMessage("Restaurant name is required.")
                .MaximumLength(100).WithMessage("Restaurant name cannot exceed 50 characters.");

            RuleFor(dto => dto.Description)
                .NotEmpty().WithMessage("Restaurant description is required.")
                .MaximumLength(500).WithMessage("Restaurant description cannot exceed 200 characters.");

            RuleFor(dto => dto.Category)
                .NotEmpty().WithMessage("Restaurant category is required.")
                .MaximumLength(50).WithMessage("Restaurant category cannot exceed 50 characters.");

            RuleFor(dto => dto.ContactEmail)
                .NotEmpty().WithMessage("Contact email is required.")
                .EmailAddress().WithMessage("Invalid email address format.");

            RuleFor(dto => dto.ContactNumber)
                .NotEmpty().WithMessage("Contact number is required.")
                .Matches(@"^[0-9]{9}$").WithMessage("Contact number should consist of nine digits.");

            RuleFor(dto => dto.BuildingNumber)
                .NotEmpty().WithMessage("Building number is required.")
                .MaximumLength(10).WithMessage("Building number cannot exceed 10 characters.");

            RuleFor(dto => dto.Street)
                .NotEmpty().WithMessage("Street name is required.")
                .MaximumLength(50).WithMessage("Street name cannot exceed 50 characters."); 

            RuleFor(dto => dto.City)
                .NotEmpty().WithMessage("City name is required.")
                .MaximumLength(50).WithMessage("City name cannot exceed 50 characters.");

            RuleFor(dto => dto.PostalCode)
                .NotEmpty().WithMessage("Postal code is required.")
                .Matches(@"^\d{2}-\d{3}$").WithMessage("Invalid postal code format. Correct format is XX-XXX.");
        }
    }
}
