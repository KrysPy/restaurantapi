﻿using FluentValidation;
using RestaurantAPI.Models;

namespace RestaurantAPI.Validators
{
    public class CreateDishDtoValidator : AbstractValidator<CreateDishDto>
    {
        public CreateDishDtoValidator()
        {
            RuleFor(d => d.Name)
                .NotEmpty().WithMessage("Name is required")
                .MaximumLength(100).WithMessage("Name is to long (max 50 chars)");

            RuleFor(d => d.Description)
                .MaximumLength(500).WithMessage("Name is to long (max 250 chars)");

            RuleFor(d => d.Price)
                .NotEmpty().WithMessage("Price is required")
                .GreaterThan(0).WithMessage("Price must be greater than 0");
        }
    }
}
