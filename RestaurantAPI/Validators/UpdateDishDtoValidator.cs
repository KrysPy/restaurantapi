﻿using FluentValidation;
using RestaurantAPI.Models;

namespace RestaurantAPI.Validators
{
    public class UpdateDishDtoValidator : AbstractValidator<UpdateDishDto>
    {
        public UpdateDishDtoValidator()
        {
            RuleFor(dto => dto.Name)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(100).WithMessage("Name cannot exceed 100 characters.");

            RuleFor(dto => dto.Description)
                .MaximumLength(500).WithMessage("Description cannot exceed 500 characters.");

            RuleFor(dto => dto.Price)
                .NotEmpty().WithMessage("Price is required.")
                .GreaterThan(0).WithMessage("Price must be greater than 0.");
        }
    }
}
