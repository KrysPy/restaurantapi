﻿using Microsoft.EntityFrameworkCore;
using RestaurantAPI.Entities;

namespace RestaurantAPI.DbContexts
{
    public class RestaurantDb : DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Dish> Dishes { get; set; }

        public RestaurantDb(DbContextOptions<RestaurantDb> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Address>()
                .Property(a => a.BuildingNumber)
                .IsRequired()
                .HasMaxLength(10);

            modelBuilder.Entity<Address>()
                .Property(a => a.Street)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Address>()
                .Property(a => a.City)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Address>()
                .Property(a => a.PostalCode)
                .IsRequired()
                .HasMaxLength(9)
                .HasAnnotation("RegularExpression", "^[0-9]{9}$");

            modelBuilder.Entity<Dish>()
                .Property(d => d.Name)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Dish>()
                .Property(d => d.Description)
                .HasMaxLength(500);

            modelBuilder.Entity<Dish>()
                .Property(d => d.Price)
                .IsRequired();

            modelBuilder.Entity<Restaurant>()
                .Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Restaurant>()
                .Property(r => r.Description)
                .HasMaxLength(500);

            modelBuilder.Entity<Restaurant>()
                .Property(r => r.Category)
                .HasMaxLength(50);

            modelBuilder.Entity<Restaurant>()
                .Property(r => r.HasDelivery)
                .IsRequired();

            modelBuilder.Entity<Restaurant>()
                .Property(r => r.ContactEmail)
                .IsRequired()
                .HasAnnotation("RegularExpression", "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$");

            modelBuilder.Entity<Restaurant>()
                .Property(r => r.ContactNumber)
                .HasAnnotation("RegularExpression", "^[0-9]+$");

            SeedDishes(modelBuilder);
            SeedAddresses(modelBuilder);
            SeedRestaurants(modelBuilder);
        }

        private void SeedRestaurants(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Restaurant>()
                .HasData(new List<Restaurant>()
            {
                new Restaurant()
                {
                    Id = 1,
                    Name = "KFC",
                    Category = "Fast Food",
                    Description =
                        "KFC (short for Kentucky Fried Chicken) is an American fast food restaurant chain headquartered in Louisville, Kentucky, that specializes in fried chicken.",
                    ContactEmail = "contact@kfc.com",
                    HasDelivery = true,
                    AddressId = 1,
                },
                new Restaurant()
                {
                    Id = 2,
                    Name = "McDonald Szewska",
                    Category = "Fast Food",
                    Description =
                        "McDonald's Corporation (McDonald's), incorporated on December 21, 1964, operates and franchises McDonald's restaurants.",
                    ContactEmail = "contact@mcdonald.com",
                    HasDelivery = true,
                    AddressId = 2,
                }
            });
        }

        private void SeedDishes(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Dish>()
                .HasData(new List<Dish>()
                    {
                        new Dish()
                        {
                            Id = 1,
                            Name = "nashville hot chicken",
                            Description = "sth",
                            Price = 10.30m,
                            RestaurantId = 2,
                        },

                        new Dish()
                        {
                            Id = 2,
                            Name = "chicken nuggets",
                            Description = "sth",
                            Price = 5.30m,
                            RestaurantId = 1,
                        },
                    });
        }

        private void SeedAddresses(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .HasData(new List<Address>
                {
                    new Address()
                    {
                        Id = 1,
                        BuildingNumber = "10/11",
                        City = "Kraków",
                        Street = "Szewska 2",
                        PostalCode = "30-001",

                    },
                    new Address()
                    {
                        Id = 2,
                        BuildingNumber = "7/10",
                        City = "Warszawa",
                        Street = "Szewska 2",
                        PostalCode = "30-001",
                    },
                });
        }
    }
}
