﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RestaurantAPI.Entities
{
    public abstract class BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}