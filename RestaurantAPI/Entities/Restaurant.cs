﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestaurantAPI.Entities
{
    public class Restaurant : BaseEntity
    {
        [ForeignKey(nameof(AddressId))]
        public Address Address { get; set; }
        public ICollection<Dish> Dishes { get; set; }
        public int AddressId { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public string? Category { get; set; }
        public bool HasDelivery { get; set; }
        public string ContactEmail { get; set; }
        public string? ContactNumber { get; set; }
    }
}
