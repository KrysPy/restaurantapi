using FluentValidation;
using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Web;
using RestaurantAPI.DbContexts;
using RestaurantAPI.Interfaces;
using RestaurantAPI.Middleware;
using RestaurantAPI.Services;
using RestaurantAPI.Validators;
using System.Text.Json.Serialization;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("Init main");

try
{
    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.

    builder.Services.AddDbContext<RestaurantDb>(
        options => options.UseSqlServer(builder.Configuration.GetConnectionString("RestaurantConnectionString"))
        );

    builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

    builder.Services.AddScoped<IRestaurantService, RestaurantService>();
    builder.Services.AddScoped<IDishService, DishService>();

    builder.Services.AddScoped<ErrorHandlingMiddleware>();
    builder.Services.AddScoped<RequestTimeMiddleware>();

    builder.Services.AddValidatorsFromAssemblyContaining<CreateDishDtoValidator>();
    builder.Services.AddValidatorsFromAssemblyContaining<CreateRestaurantDtoValidator>();
    builder.Services.AddValidatorsFromAssemblyContaining<UpdateDishDtoValidator>();
    builder.Services.AddValidatorsFromAssemblyContaining<UpdateRestaurantDtoValidator>();

    // NLog settings
    builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
    builder.Host.UseNLog();

    builder.Services.AddControllers().AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        options.JsonSerializerOptions.WriteIndented = true;
    });

    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    var app = builder.Build();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();

        using var scope = app.Services.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<RestaurantDb>();
        context.Database.Migrate();
    }

    app.UseMiddleware<ErrorHandlingMiddleware>();
    app.UseMiddleware<RequestTimeMiddleware>();

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();

}
catch (Exception exception)
{
    // NLog: catch setup errors
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    NLog.LogManager.Shutdown();
}
