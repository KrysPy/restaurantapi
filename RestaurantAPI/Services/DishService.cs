﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RestaurantAPI.DbContexts;
using RestaurantAPI.Entities;
using RestaurantAPI.Interfaces;
using RestaurantAPI.Models;

namespace RestaurantAPI.Services
{
    public class DishService : IDishService
    {
        private readonly RestaurantDb _dbContext;
        private readonly IMapper _mapper;

        public DishService(RestaurantDb dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public DishDto? CreateDish(int restaurantId, CreateDishDto dto)
        {
            var restaurantExists = _dbContext.Restaurants
                .AsNoTracking()
                .Any(r => r.Id == restaurantId);

            if (!restaurantExists)
            {
                return null;
            }

            var dish = _mapper.Map<Dish>(dto);

            dish.RestaurantId = restaurantId;

            _dbContext.Dishes.Add(dish);
            _dbContext.SaveChanges();

            return _mapper.Map<DishDto?>(dish);
        }

        public IEnumerable<DishDto>? GetAllRestaurantDishes(int restaurantId)
        {
            var restaurantExists = _dbContext.Restaurants
                .AsNoTracking()
                .Any(r => r.Id == restaurantId);

            if (!restaurantExists)
            {
                return null;
            }

            var restaurantDishes = _dbContext.Dishes
                .AsNoTracking()
                .Where(r => r.RestaurantId == restaurantId)
                .ToList();

            return _mapper.Map<IEnumerable<DishDto>?>(restaurantDishes);
        }

        public DishDto? GetDishById(int restaurantId, int dishId)
        {
            var restaurantExists = _dbContext.Restaurants
                .AsNoTracking()
                .Any(r => r.Id == restaurantId);

            if (!restaurantExists)
            {
                return null;
            }

            var dish = _dbContext.Dishes
                .AsNoTracking()
                .FirstOrDefault(d => d.Id == dishId);

            if (dish is null)
            {
                return null;
            }
            return _mapper.Map<DishDto?>(dish);
        }

        public bool UpdateDish(int restaurantId, int dishId, UpdateDishDto dto)
        {
            var restaurantExists = _dbContext.Restaurants
                .AsNoTracking()
                .Any(r => r.Id == restaurantId);

            if (!restaurantExists)
            {
                return false;
            }

            var dish = _dbContext.Dishes.FirstOrDefault(d => d.Id == dishId);

            if (dish is null)
            {
                return false;
            }

            var updatedDish = _mapper.Map(dto, dish);

            _dbContext.Update(updatedDish);
            _dbContext.SaveChanges();

            return true;
        }

        public bool DeleteDish(int restaurantId, int dishId)
        {
            var restaurantExists = _dbContext.Restaurants
                .AsNoTracking()
                .Any(r => r.Id == restaurantId);

            if (!restaurantExists)
            {
                return false;
            }

            var dish = _dbContext.Dishes.FirstOrDefault(d => d.Id == dishId);

            if (dish is null)
            {
                return false;
            }

            _dbContext.Dishes.Remove(dish);
            _dbContext.SaveChanges();

            return true;
        }

        public bool DeleteAllDishes(int restaurantId)
        {
            var restaurantExists = _dbContext.Restaurants
                .AsNoTracking()
                .Any(r => r.Id == restaurantId);

            if (!restaurantExists) 
            {
                return false;
            }
            _dbContext.Dishes.RemoveRange(_dbContext.Dishes.Where(d => d.RestaurantId == restaurantId));
            _dbContext.SaveChanges();

            return true;
        }
    }
}
