﻿using System.Diagnostics;

namespace RestaurantAPI.Middleware
{
    public class RequestTimeMiddleware : IMiddleware
    {
        private readonly ILogger<RequestTimeMiddleware> _logger;

        public RequestTimeMiddleware(ILogger<RequestTimeMiddleware> logger)
        {
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var timer = new Stopwatch();

            timer.Start();
            await next.Invoke(context);
            timer.Stop();

            var elapsedTime = timer.ElapsedMilliseconds;

            if (elapsedTime / 1000 > 4) 
            {
                var message = $"HTTP method: {context.Request.Method} at {context.Request.Path} took {elapsedTime} ms";
                _logger.LogWarning(message);
            }
        }
    }
}
