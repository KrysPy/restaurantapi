﻿using AutoMapper;
using RestaurantAPI.Models;
using RestaurantAPI.Entities;

namespace RestaurantAPI.Mappings.Profiles
{
    public class RestaurantMappingProfile : Profile
    {
        public RestaurantMappingProfile()
        {
            CreateMap<Restaurant, RestaurantDto>()
                .ForMember(m => m.BuildingNumber, c => c.MapFrom(s => s.Address.BuildingNumber))
                .ForMember(m => m.City, c => c.MapFrom(s => s.Address.City))
                .ForMember(m => m.Street, c => c.MapFrom(s => s.Address.Street))
                .ForMember(m => m.PostalCode, c => c.MapFrom(s => s.Address.PostalCode));

            CreateMap<CreateRestaurantDto, Restaurant>()
                .ForMember(r => r.Address, c => c.MapFrom(dto => new Address()
                {
                    BuildingNumber = dto.BuildingNumber,
                    City = dto.City,
                    PostalCode = dto.PostalCode,
                    Street = dto.Street,
                }));

            CreateMap<UpdateRestaurantDto, Restaurant>();
        }
    }
}
