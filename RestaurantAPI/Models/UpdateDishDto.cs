﻿namespace RestaurantAPI.Models
{
    public class UpdateDishDto
    {
        public string Name { get; init; }
        public string? Description { get; init; }
        public decimal Price { get; init; }
    }
}
