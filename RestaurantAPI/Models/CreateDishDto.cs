﻿using System.ComponentModel.DataAnnotations;

namespace RestaurantAPI.Models
{
    public class CreateDishDto
    {
        public string Name { get; init; }
        public string? Description { get; init; }
        public decimal Price { get; init; }
    }
}
