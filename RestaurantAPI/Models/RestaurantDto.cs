﻿namespace RestaurantAPI.Models
{
    public class RestaurantDto
    {
        public int Id { get; init; }
        public string Name { get; init; }
        public string Description { get; init; }
        public string Category { get; init; }
        public bool HasDelivery { get; init; }
        public string BuildingNumber { get; init; }
        public string City { get; init; }
        public string Street { get; init; }
        public string PostalCode { get; init; }
        public List<DishDto> Dishes { get; init; }
    }
}
